# @atlaskit/app-switcher

## 9.1.0 (2018-10-29)

* feature; use specific permission checks in app switcher rather than relying on generic "isTr ([9bad14d](https://bitbucket.org/atlassian/atlaskit/commits/9bad14d))
## 9.0.0 (2018-10-23)

* feature; clean up unused props in App Switcher. Clean up storybook. ([29fe6f3](https://bitbucket.org/atlassian/atlaskit/commits/29fe6f3))

* breaking; none ([1384aa1](https://bitbucket.org/atlassian/atlaskit/commits/1384aa1))
* breaking; eUE-673 Remove unused code for Invite Users in App Switcher component ([1384aa1](https://bitbucket.org/atlassian/atlaskit/commits/1384aa1))

## 8.1.0 (2018-10-22)


* feature; display the "Site administration" link and the "Discover applications" link for bot (issues closed: eue-675) ([90f0f80](https://bitbucket.org/atlassian/atlaskit/commits/90f0f80))
## 8.0.1 (2018-08-14)

* bug fix; adding small padding-top to marketplace app switcher icon (issues closed: mc-239) ([6572ba6](https://bitbucket.org/atlassian/atlaskit/commits/6572ba6))
## 8.0.0 (2018-08-14)

* breaking; Property \`marketplaceLink\` of app-switcher no longer has an \`icon\` field ([8516a59](https://bitbucket.org/atlassian/atlaskit/commits/8516a59))
* breaking; updated AppSwitcher to render its own MarketplaceIcon, whenever marketplace icon sh (issues closed: mc-239) ([8516a59](https://bitbucket.org/atlassian/atlaskit/commits/8516a59))
## 7.1.0 (2018-08-09)

* feature; Adds Marketplace apps link to App switcher (issues closed: mc-239) ([7d88712](https://bitbucket.org/atlassian/atlaskit/commits/7d88712))
## 7.0.0 (2018-07-19)




* breaking; "appswitcher.trigger.click" analytics event was renamed to "appswitcher.trigger.opened" ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))
* breaking; nEXT-6027 app-switcher - make analytics event names consistent (issues closed: next-6027) ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))

* feature; NEXT-6005 Add onClose prop to app-switcher atlaskit component (issues closed: next-6005) ([fe19ad9](https://bitbucket.org/atlassian/atlaskit/commits/fe19ad9))
## 6.0.0 (2018-07-18)



* breaking; "appswitcher.trigger.click" analytics event was renamed to "appswitcher.trigger.opened" ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))
* breaking; nEXT-6027 app-switcher - make analytics event names consistent (issues closed: next-6027) ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))

* feature; NEXT-6005 Add onClose prop to app-switcher atlaskit component (issues closed: next-6005) ([fe19ad9](https://bitbucket.org/atlassian/atlaskit/commits/fe19ad9))
## 5.0.0 (2018-07-18)


* breaking; "appswitcher.trigger.click" analytics event was renamed to "appswitcher.trigger.opened" ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))
* breaking; nEXT-6027 app-switcher - make analytics event names consistent (issues closed: next-6027) ([d6d489b](https://bitbucket.org/atlassian/atlaskit/commits/d6d489b))

* feature; NEXT-6005 Add onClose prop to app-switcher atlaskit component (issues closed: next-6005) ([fe19ad9](https://bitbucket.org/atlassian/atlaskit/commits/fe19ad9))
## 4.0.2 (2018-06-21)


* bug fix; updated AtlasKit dependencies to their latest versions. ([5407eb3](https://bitbucket.org/atlassian/atlaskit/commits/5407eb3))
## 4.0.1 (2018-06-18)

* bug fix; removed grow0 events ([27efc0a](https://bitbucket.org/atlassian/atlaskit/commits/27efc0a))
## 4.0.0 (2018-06-18)

* breaking; ' ([8ae57f4](https://bitbucket.org/atlassian/atlaskit/commits/8ae57f4))
* breaking; adding inviteUsersLink to appSwitcher (issues closed: eue-441) ([8ae57f4](https://bitbucket.org/atlassian/atlaskit/commits/8ae57f4))
## 3.8.0 (2018-06-12)

* feature; changing analytic events (issues closed: eue-559) ([7bc7a18](https://bitbucket.org/atlassian/atlaskit/commits/7bc7a18))
## 3.7.0 (2018-06-06)

* bug fix; change ordering of top links in app-switcher ([18340cd](https://bitbucket.org/atlassian/atlaskit/commits/18340cd))

* feature; add people profile link to app-switcher (issues closed: ak-4805) ([d11c7cf](https://bitbucket.org/atlassian/atlaskit/commits/d11c7cf))

## 3.6.3 (2018-04-27)

* bug fix; i18n discover.products as optional (issues closed: eue-240) ([90d3b9b](https://bitbucket.org/atlassian/atlaskit/commits/90d3b9b))
## 3.6.2 (2018-04-27)

* bug fix; setting discoverProductsLink on app-switcher as optional (issues closed: eue-240) ([463014e](https://bitbucket.org/atlassian/atlaskit/commits/463014e))
## 3.6.1 (2018-04-26)

* bug fix; refactor Discover products link to be Discover applications ([04b63f7](https://bitbucket.org/atlassian/atlaskit/commits/04b63f7))
## 3.6.0 (2018-04-26)



* feature; adding optional discover products link to app-switcher (issues closed: eue-240) ([9a0a8d9](https://bitbucket.org/atlassian/atlaskit/commits/9a0a8d9))
## 3.5.1 (2018-04-23)

* bug fix; cLEM-1641 app-switcher now support styled-components v3 (issues closed: http://product-fabric.atlassian.net/browse/clem-1641) ([9a2d648](https://bitbucket.org/atlassian/atlaskit/commits/9a2d648))
## 3.5.0 (2018-02-21)


* bug fix; change link analytics identifier property name to "analyticsRef" ([02ea33c](https://bitbucket.org/atlassian/atlaskit/commits/02ea33c))

* bug fix; revert removal of previous commit ([58055a5](https://bitbucket.org/atlassian/atlaskit/commits/58055a5))
* feature; add analytics identifier "ref" prop to links ([64308df](https://bitbucket.org/atlassian/atlaskit/commits/64308df))

* bug fix; fixed prop type of links ([124b7d1](https://bitbucket.org/atlassian/atlaskit/commits/124b7d1))
* bug fix; undo changes from other branch accidentally committed ([e304300](https://bitbucket.org/atlassian/atlaskit/commits/e304300))
* feature; add support for arbitrary hyperlinks at the bottom of the app switcher ([fd46021](https://bitbucket.org/atlassian/atlaskit/commits/fd46021))
## 3.4.0 (2018-02-15)

* bug fix; add prop types and flow types ([73ec073](https://bitbucket.org/atlassian/atlaskit/commits/73ec073))



* bug fix; change linked application "subtitle" to "label" ([e94ee70](https://bitbucket.org/atlassian/atlaskit/commits/e94ee70))
* feature; add optional subtitle text to linked applications ([96832be](https://bitbucket.org/atlassian/atlaskit/commits/96832be))



## 3.3.0 (2017-12-13)

* bug fix; add required homeLink argument to getTopLinks() tests (issues closed: nav-35) ([e89f924](https://bitbucket.org/atlassian/atlaskit/commits/e89f924))

* feature; add new prop for custom Home link to app-switcher (issues closed: nav-35) ([759b032](https://bitbucket.org/atlassian/atlaskit/commits/759b032))

## 3.2.2 (2017-11-14)

* bug fix; bumping internal dependencies to latest major versions ([c9f1af6](https://bitbucket.org/atlassian/atlaskit/commits/c9f1af6))



## 3.2.1 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))

## 3.2.0 (2017-09-12)

* feature; replace Home icon with Atlassian logo (issues closed: home-983) ([4f2a1c6](https://bitbucket.org/atlassian/atlaskit/commits/4f2a1c6))
## 3.1.1 (2017-09-06)





* bug fix; use @atlaskit/avatar for recent container icons (issues closed: home-884) ([482af6d](https://bitbucket.org/atlassian/atlaskit/commits/482af6d))
## 3.1.0 (2017-08-23)

* feature; aK-3356 Allow the AppSwitcher to close on click (issues closed: ak-3356) ([2710d52](https://bitbucket.org/atlassian/atlaskit/commits/2710d52))
## 3.0.0 (2017-08-22)

* feature; modify presentation and API of suggested applications. (issues closed: ak-3337) ([b8b650f](https://bitbucket.org/atlassian/atlaskit/commits/b8b650f))
* feature; modify presentation and API of suggested applications (issues closed: ak-3337) ([35f4d93](https://bitbucket.org/atlassian/atlaskit/commits/35f4d93))
* breaking; suggestedApplications prop type is no longer supported. ([b9ac0d1](https://bitbucket.org/atlassian/atlaskit/commits/b9ac0d1))
* breaking; modify presentation and API of suggested applications ([b9ac0d1](https://bitbucket.org/atlassian/atlaskit/commits/b9ac0d1))
* feature; modify presentation and API of suggested applications ([61da9ef](https://bitbucket.org/atlassian/atlaskit/commits/61da9ef))






## 2.5.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.5.0 (2017-06-23)


* feature; add optional "Site Admin" link to `app-switcher` ([89a4e20](https://bitbucket.org/atlassian/atlaskit/commits/89a4e20))

## 2.4.3 (2017-06-14)


* fix; update internal components to latest dropdown-menu ([ad63284](https://bitbucket.org/atlassian/atlaskit/commits/ad63284))

## 2.4.2 (2017-06-08)


* fix; fix app-switcher props suggestedApplication.application and suggestedApplication.onD ([fe7d854](https://bitbucket.org/atlassian/atlaskit/commits/fe7d854))
* fix; bump [@atlaskit](https://github.com/atlaskit)/logo dependency in app-switcher ([bb9fd6e](https://bitbucket.org/atlassian/atlaskit/commits/bb9fd6e))

## 2.4.1 (2017-06-06)


* fix; move [@atlaskit](https://github.com/atlaskit)/util-readme to devDependencies ([fbe50c0](https://bitbucket.org/atlassian/atlaskit/commits/fbe50c0))

## 2.4.0 (2017-05-26)

## 2.3.5 (2017-05-26)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))


* feature; add isLoading to app-switcher ([fed0490](https://bitbucket.org/atlassian/atlaskit/commits/fed0490))

## 2.3.4 (2017-05-10)


* fix; testing releasing more than 5 packages at a time ([e69b832](https://bitbucket.org/atlassian/atlaskit/commits/e69b832))

## 2.3.3 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.3.2 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.3.1 (2017-04-20)


* fix; upgrade droplist dependency version ([0dd084d](https://bitbucket.org/atlassian/atlaskit/commits/0dd084d))


null temporarily revert changes ([8d22c2d](https://bitbucket.org/atlassian/atlaskit/commits/8d22c2d))

## 2.3.0 (2017-04-05)


* feature; add max height to app switcher dropdown ([73a1795](https://bitbucket.org/atlassian/atlaskit/commits/73a1795))
* feature; depend on latest versions of dropdown-menu and droplist ([03d3423](https://bitbucket.org/atlassian/atlaskit/commits/03d3423))
* feature; fire analytics event when app switcher is opened ([730bb26](https://bitbucket.org/atlassian/atlaskit/commits/730bb26))
* feature; show only the first 6 recent containers ([04efe38](https://bitbucket.org/atlassian/atlaskit/commits/04efe38))

## 2.2.0 (2017-03-23)


* feature; add prop to disable Home link ([d5c89a1](https://bitbucket.org/atlassian/atlaskit/commits/d5c89a1))

## 2.1.5 (2017-03-21)

## 2.1.5 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 2.1.4 (2017-03-14)

## 2.1.3 (2017-03-07)


* fix; tidy up line spacing and move to correct min-width ([6071322](https://bitbucket.org/atlassian/atlaskit/commits/6071322))

## 2.1.2 (2017-03-06)


* fix; change app switcher dropdown width to 257px ([1de99a2](https://bitbucket.org/atlassian/atlaskit/commits/1de99a2))

## 2.1.1 (2017-03-02)


* fix; remove non-i81n "Configure" text ([386748c](https://bitbucket.org/atlassian/atlaskit/commits/386748c))
* fix; dummy commit to re-release storybook ([e3c58b7](https://bitbucket.org/atlassian/atlaskit/commits/e3c58b7))

## 2.1.0 (2017-03-01)


* fix; change i18n PropTypes to "node" rather than "string" ([c307296](https://bitbucket.org/atlassian/atlaskit/commits/c307296))


* feature; allow StatelessDropdownMenu props to be overridden ([10801a7](https://bitbucket.org/atlassian/atlaskit/commits/10801a7))

## 2.0.1 (2017-02-27)


* empty commit to make components release themselves ([5511fbe](https://bitbucket.org/atlassian/atlaskit/commits/5511fbe))

## 1.0.0 (2017-02-16)


* Allow application description to word wrap ([97e4ab7](https://bitbucket.org/atlassian/atlaskit/commits/97e4ab7))
* Change Home icon SVG to inherit color ([2eeb814](https://bitbucket.org/atlassian/atlaskit/commits/2eeb814))
* Fix line-height usage ([b4f1a73](https://bitbucket.org/atlassian/atlaskit/commits/b4f1a73))
* Mark AppSwitcher props as required ([69ecbe1](https://bitbucket.org/atlassian/atlaskit/commits/69ecbe1))
* Move RecentContainerType on grid ([98d705e](https://bitbucket.org/atlassian/atlaskit/commits/98d705e))
* Tidy CSS ([eda6e76](https://bitbucket.org/atlassian/atlaskit/commits/eda6e76))
* Tidy up min/max width of the appswitcher ([5c305a4](https://bitbucket.org/atlassian/atlaskit/commits/5c305a4))
* Use scoped packages ([89bbb5d](https://bitbucket.org/atlassian/atlaskit/commits/89bbb5d))


* Add analytics to app switcher actions ([eba56ec](https://bitbucket.org/atlassian/atlaskit/commits/eba56ec))
* Add analytics to the RecentContainers component ([84fb7ad](https://bitbucket.org/atlassian/atlaskit/commits/84fb7ad))
* Add anonymous mode to the app switcher ([c1956b2](https://bitbucket.org/atlassian/atlaskit/commits/c1956b2))
* Add AppSwitcher component ([b6eabf5](https://bitbucket.org/atlassian/atlaskit/commits/b6eabf5))
* Add error state for application links ([979ece9](https://bitbucket.org/atlassian/atlaskit/commits/979ece9))
* Add README story to app-switcher ([efcdc2e](https://bitbucket.org/atlassian/atlaskit/commits/efcdc2e))
* Switch to [@atlaskit](https://github.com/atlaskit)/logo ([f0ed039](https://bitbucket.org/atlassian/atlaskit/commits/f0ed039))
