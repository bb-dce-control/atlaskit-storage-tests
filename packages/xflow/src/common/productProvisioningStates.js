export const ACTIVE = Symbol('ACTIVE');
export const ACTIVATING = Symbol('ACTIVATING');
export const INACTIVE = Symbol('INACTIVE');
export const DEACTIVATED = Symbol('DEACTIVATED');
export const UNKNOWN = Symbol('UNKNOWN');
