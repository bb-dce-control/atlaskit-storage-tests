import styled from 'styled-components';

import { gridSize, math } from '@atlaskit/theme';

const WhereToFindConfluenceImg = styled.img`
  height: ${math.multiply(gridSize, 16)}px;
  width: ${math.multiply(gridSize, 16)}px;
`;

WhereToFindConfluenceImg.displayName = 'WhereToFindConfluenceImg';
export default WhereToFindConfluenceImg;
